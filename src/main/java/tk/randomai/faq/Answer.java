/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tk.randomai.faq;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLEncoder;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import tk.randomai.faq.beans.Question;
import tk.randomai.faq.dao.DaoFactory;
import tk.randomai.faq.dao.QuestionDao;
import tk.randomai.faq.util.Mail;
import tk.randomai.faq.util.MdToHtml;

/**
 *
 * @author tobi
 */
public class Answer extends HttpServlet {

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // Verification de la connexion active de l'admin
        String from = request.getRequestURL().toString();
        String _context = request.getScheme() + "://"
                + request.getServerName()
                + ("http".equals(request.getScheme()) && request.getServerPort() == 80
                || "https".equals(request.getScheme())
                && request.getServerPort() == 443 ? "" : ":"
                + request.getServerPort())
                + request.getContextPath();
        if (request.getSession().getAttribute("user") == null) {
            String url = _context + "/management/login?from="
                    + URLEncoder.encode(from);
            response.sendRedirect(url);
        }
        int id = -1;
        if (request.getParameter("q") == null) {
            request
                    .getSession()
                    .setAttribute("notification", "Aucune question");
            response.sendRedirect(_context + "/management/dashboard");
        }
        String question_id = request.getParameter("q");
        try {
            id = Integer.parseInt(question_id);
        }
        catch (Exception e) {
            // nothing
            request.setAttribute("notification", "Question non trouvee!");
            this.getServletContext()
                    .getRequestDispatcher("/WEB-INF/answer.jsp")
                    .forward(request, response);
        }
        DaoFactory factory = (DaoFactory) getServletContext()
                .getAttribute("dao_factory");
        QuestionDao qDao = factory.getQuestionDao();
        Question question = qDao.getById(id);
        if (question == null) {
            request.setAttribute("notification", "Question non trouvee!");
            response.sendRedirect(_context + "/management/dashboard");
        }
        String context = request.getScheme() + "://"
                + request.getServerName()
                + ("http".equals(request.getScheme()) && request.getServerPort() == 80
                || "https".equals(request.getScheme())
                && request.getServerPort() == 443 ? "" : ":"
                + request.getServerPort())
                + request.getContextPath();
        request.setAttribute("contextPath", context);
        request.setAttribute("question", question);
        this.getServletContext()
                .getRequestDispatcher("/WEB-INF/answer.jsp")
                .forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if (request.getParameter("submit") != null) {
            String context = request.getScheme() + "://"
                    + request.getServerName()
                    + ("http".equals(request.getScheme()) && request.getServerPort() == 80
                    || "https".equals(request.getScheme())
                    && request.getServerPort() == 443 ? "" : ":"
                    + request.getServerPort())
                    + request.getContextPath();
            String idString = request.getParameter("q");
            int id = 0;
            try {
                id = Integer.parseInt(idString);
            }
            catch (Exception e) {
                // nothing
                request.getSession().setAttribute("notification", "Question non trouvee!");
                response.sendRedirect(context + "/management/dashboard");
            }
            String answer = request.getParameter("answer");
            String tags = request.getParameter("tagging");
            String[] tagsArray = tags.split("\\s+|,\\s?");
            DaoFactory factory = (DaoFactory) getServletContext()
                    .getAttribute("dao_factory");
            QuestionDao qDao = factory.getQuestionDao();
            Question question = qDao.getById(id);
            if (question == null) {
                request.getSession().setAttribute("notification", "Question non trouvee!");
                response.sendRedirect(context + "/management/dashboard");
            }
            answer = MdToHtml.toHtml(answer);
            qDao.update(answer, question);
            for (int i = 0; i < tagsArray.length; i++) {
                try {
                    qDao.setTag(tagsArray[i], question);
                }
                catch (Exception ex) {
                    Logger.getLogger(Answer.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (question.getAuthor_email() != null && !question.getAuthor_email().isEmpty()) {
                Mail.sendMail(question.getAuthor_email(),
                        String
                                .format("Reponse a votre question en date du %s", question.getCreated_at()),
                        answer
                );
            }
            request.getSession().setAttribute("notification", "Question erpondue avec succes!");
            response.sendRedirect(context + "/management/dashboard");
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
