/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tk.randomai.faq.beans;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author kadou
 */
@Entity
@Table(name = "questions")
@NamedQueries({
    @NamedQuery(
        name = "select_answered",
        query = "SELECT q FROM questions q WHERE q.answer IS NOT NULL LIMIT :lim OFFSET :off"
    ),
    @NamedQuery(
        name = "count not answered",
        query = "SELECT COUNT(q) FROM questions q WHERE q.answer IS NULL"
    ),
    @NamedQuery(
        name = "select_not_answered",
        query = "SELECT q FROM questions q WHERE q.answer IS NULL LIMIT :lim OFFSET :off"
    ),
    @NamedQuery(
        name = "delete",
        query = "DELETE FROM questions q WHERE q.id = :givenId"
    )
})
public class Question implements Serializable {
    
    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;
    
    @Column(name = "title")
    private String title;
    
    @Column(name = "body")
    private String body;
    
    @ManyToMany
    @JoinTable(name="questions_tags",
        joinColumns = @JoinColumn(name = "question"), 
        inverseJoinColumns = @JoinColumn(name = "tag"))
    private List<Tag> tags;
    
    @Column(name = "answer")
    private String answer;
    
    @Column(name = "author_email")
    private String author_email;
    
    @Column(name = "created_at")
    private String created_at;
    
    @Column(name = "updated_at")
    private String updated_at;
    
    @ManyToOne
    @JoinColumn(name = "id")
    private Admin updated_by;
    
    @Column(name = "answered_at")
    private String answered_at;
    
    @ManyToOne
    @JoinColumn(name = "id")
    private Admin answered_by;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    } 

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getAuthor_email() {
        return author_email;
    }

    public void setAuthor_email(String author_email) {
        this.author_email = author_email;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public Admin getUpdated_by() {
        return updated_by;
    }

    public void setUpdated_by(Admin updated_by) {
        this.updated_by = updated_by;
    }

    public String getAnswered_at() {
        return answered_at;
    }

    public void setAnswered_at(String answered_at) {
        this.answered_at = answered_at;
    }

    public Admin getAnswered_by() {
        return answered_by;
    }

    public void setAnswered_by(Admin answered_by) {
        this.answered_by = answered_by;
    }
}
