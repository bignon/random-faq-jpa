package tk.randomai.faq.dao;

import java.io.InputStream;
import java.io.IOException;
import java.util.Properties;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import tk.randomai.faq.dao.exceptions.DaoConfigException;
import tk.randomai.faq.dao.exceptions.DaoException;

/**
 *
 * @author kadou
 */
public class DaoFactory {

    private static final String DB_URL_ENV = "JDBC_DATABASE_URL";
    private static final String DB_DRIVER_ENV = "JDBC_DRIVER";
    private static final String PROPERTIES_FILE = "dao.properties";
    private static final String URL_PROPERTY = "db_url";
    private static final String DRIVER_PROPERTY = "driver";

    private String url;

    public DaoFactory(String url) {
        this.url = url;
    }

    /**
     * Method to get a DaoFactory.
     *
     * @return DaoFactory
     */
    public static DaoFactory getInstance() throws DaoConfigException {
        Properties props = new Properties();
        String url = System.getenv(DB_URL_ENV);
        String driver = System.getenv(DB_DRIVER_ENV);
        if (url != null && driver != null) {
            try {
                Class.forName(driver);
            }
            catch (ClassNotFoundException e) {
                throw new DaoConfigException("Driver " + driver + "not found\n",
                    e);
            }
            return new DaoFactory(url);
        }
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        InputStream properties_file = loader.getResourceAsStream(
                PROPERTIES_FILE);

        if (properties_file == null) {
            throw new DaoConfigException("Unable to find properties file"
                    + ": " + PROPERTIES_FILE + "\n");
        }

        try {
            props.load(properties_file);
            url = props.getProperty(URL_PROPERTY);
            driver = props.getProperty(DRIVER_PROPERTY);
        }
        catch (IOException e) {
            throw new DaoConfigException("Unable to load properties "
                    + "file: " + PROPERTIES_FILE + "\n", e);
        }

        try {
            Class.forName(driver);
        }
        catch (ClassNotFoundException e) {
            throw new DaoConfigException("Driver " + driver + "not found\n",
                    e);
        }

        return new DaoFactory(url);
    }

    /**
     * This method returns a connection object to the database
     * @return Connection, the connection object
     */
    public Connection connect() {
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(url);
        }
        catch (SQLException e) {
            throw new DaoException("An error occured", e);
        }

        return connection;
    }

    /**
     * Method to get a QuestionDao object
     * @return QuestionDao
     */
    public QuestionDao getQuestionDao() {
        return new QuestionDao(this);
    }

    public AdminDao getAdminDao() {
        return new AdminDao();
    }
    
    public TagDao getTagDao() {
        return new TagDao(this);
    }
    
    /*
    * Helper methods
     */
    /**
     * Prepare a SQL Statement
     *
     * @param connection, the connection to the database
     * @param returnGeneratedKeys, whether the preparedStatement should return
     * generated keys or not
     * @param sqlQuery, the query string
     * @param args, the args for the query placeholders
     * @return PreparedStatement, the resulting prepared statement
     * @throws SQLException
     */
    public static PreparedStatement initPreparedStatement(Connection connection,
            boolean returnGeneratedKeys, String sqlQuery,
            Object... args) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(sqlQuery,
                returnGeneratedKeys ? Statement.RETURN_GENERATED_KEYS
                        : Statement.NO_GENERATED_KEYS);

        for (int i = 1; i <= args.length; i++) {
            statement.setObject(i, args[i - 1]);
        }

        return statement;
    }

    /**
     * Close a given connection to database
     *
     * @param connection
     */
    public static void closeSilently(Connection connection) {
        if (connection != null) {
            try {
                connection.close();
            }
            catch (SQLException e) {
                // Ignore exception
            }
        }
    }

    /**
     * Close a given prepared statement
     *
     * @param statement
     */
    public static void closeSilently(PreparedStatement statement) {
        if (statement != null) {
            try {
                statement.close();
            }
            catch (SQLException e) {
                // Ignore exception
            }
        }
    }

    /**
     * Close a given result set
     *
     * @param set
     */
    public static void closeSilently(ResultSet set) {
        if (set != null) {
            try {
                set.close();
            }
            catch (SQLException e) {
                // Ignore exception
            }
        }
    }

    /**
     * Close silently
     *
     * @param connection
     * @param statement
     */
    public static void closeSilently(Connection connection,
            PreparedStatement statement) {
        closeSilently(connection);
        closeSilently(statement);
    }

    /**
     * Close silently
     *
     * @param connection
     * @param statement
     * @param set
     */
    public static void closeSilently(Connection connection,
            PreparedStatement statement, ResultSet set) {
        closeSilently(connection);
        closeSilently(statement);
        closeSilently(set);
    }
}
