package tk.randomai.faq.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import tk.randomai.faq.beans.Question;
import tk.randomai.faq.beans.Tag;
import tk.randomai.faq.dao.exceptions.DaoException;
import tk.randomai.faq.dao.interfaces.QuestionDaoInterface;
import tk.randomai.faq.util.MdToHtml;

/**
 *
 * @author kadou
 */
public class QuestionDao implements QuestionDaoInterface {

    private static final String CREATE_QUESTION_SQL_QUERY = "INSERT INTO questions("
            + "title, body, author_email) VALUES (?, ?, ?)";
    private static final String DELETE_QUESTION_SQL_QUERY = "DELETE FROM "
            + "questions WHERE id = ?";
    private static final String FIND_BY_ID_SQL_QUERY = "SELECT * FROM questions"
            + " WHERE id = ?";
    private static final String SELECT_QUESTIONS_QUERY = "SELECT * FROM questions "
            + "WHERE answer IS NOT NULL LIMIT ? OFFSET ?";
    private static final String SELECT_NOT_ANSWERED_QUESTIONS_QUERY = "SELECT * FROM questions "
            + "WHERE answer IS NULL LIMIT ? OFFSET ?";
    private static final String SELECT_QUESTION_TAGS = "SELECT tag FROM questions_tags "
            + "WHERE question = ?";
    private static String COUNT_SQL_QUERY = "SELECT COUNT(*) FROM questions WHERE answer IS NULL;";
    private static String UPDATE_QUESTION_ANSWER_SQL_QUERY = "UPDATE questions "
            + "SET "
            + " answer = ? "
            + "WHERE id = ?";
    private static final String ADD_TAG_QUERY = "INSERT INTO questions_tags VALUES (?, ?)";
    private static String FIND_QUESTIONS_WITH_TAGS = "SELECT * FROM questions_tags WHERE ";
    private DaoFactory factory;

    @PersistenceContext
    private EntityManager em;
    
    public QuestionDao(DaoFactory factory) {
        this.factory = factory;
    }

    @Override
    public List<Question> getQuestions(int limit, int offset, boolean answered)
            throws DaoException {
        List<Question> result = new ArrayList<>();
        Connection connection = null;
        try {
            connection = factory.connect();
        }
        catch (DaoException e) {
            throw e;
        }
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            if (answered) {
                statement = DaoFactory.initPreparedStatement(connection,
                        true, SELECT_QUESTIONS_QUERY, limit, offset);
            } else {
                statement = DaoFactory.initPreparedStatement(connection,
                        true, SELECT_NOT_ANSWERED_QUESTIONS_QUERY, limit, offset);
            }
            resultSet = statement.executeQuery();
            result = mapToList(resultSet);
        }
        catch (SQLException e) {
            throw new DaoException("Something went wrong", e);
        }
        return result;
    }

    @Override
    public List<String> getTags(Question question) throws DaoException {
        List<String> tags = new ArrayList<>();
        for (int i = 0; i < question.getTags().size(); i++) {
            tags.add(question.getTags().get(i).getTag());
        }
        return tags;
    }

    @Override
    public List<Question> getQuestions(int limit, int offset, List<String> tags)
            throws DaoException {
        List<Question> result = new ArrayList<>();
        if (tags.size() < 1 || tags == null) {
            return result;
        }
        Connection connection = null;
        try {
            connection = factory.connect();
        }
        catch (DaoException e) {
            throw e;
        }
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            // Complement de la chaine
            String next = "";

            for (int i = 0; i < tags.size(); i++) {
                if (i == tags.size() - 1) {
                    next += "tag = ? ";
                    continue;
                }
                next += "tag = ? OR ";
            }
            next += "LIMIT ? OFFSET ?";
            System.out.println(FIND_QUESTIONS_WITH_TAGS + next);
            statement = connection.prepareStatement(FIND_QUESTIONS_WITH_TAGS + next, Statement.NO_GENERATED_KEYS);
            for (int i = 1; i <= tags.size(); i++) {
                statement.setObject(i, "#" + tags.get(i - 1));
            }
            statement.setObject(tags.size() + 1, limit);
            statement.setObject(tags.size() + 2, offset);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                result.add(getById((int) resultSet.getLong("question")));
            }
        }
        catch (SQLException e) {
            DaoFactory.closeSilently(connection, statement, resultSet);
            throw new DaoException("Something went wrong", e);
        }
        return result;
    }

    @Override
    public void store(Question question) throws DaoException {
        Connection connection = null;
        try {
            connection = factory.connect();
        }
        catch (DaoException ex) {
            throw ex;
        }
        PreparedStatement statement = null;
        ResultSet generatedKeys = null;
        int status;
        try {
            statement = DaoFactory.initPreparedStatement(connection,
                    true, CREATE_QUESTION_SQL_QUERY,
                    question.getTitle(),
                    question.getBody(),
                    question.getAuthor_email());
        }
        catch (SQLException e) {
            throw new DaoException("Something wents wrong", e);
        }

        try {
            status = statement.executeUpdate();
        }
        catch (SQLException e) {
            throw new DaoException("Unable to create new question.", e);
        }

        if (status == 0) {
            throw new DaoException("Unable to create new question");
        }

        try {
            generatedKeys = statement.getGeneratedKeys();
        }
        catch (SQLException e) {
            throw new DaoException("Unable to create new question:"
                    + " no id generated");
        }
        catch (DaoException e) {
            throw e;
        }
        DaoFactory.closeSilently(connection, statement, generatedKeys);
    }

    @Override
    public Question getById(int id) {
        Question question = null;
        Connection connection = null;
        try {
            connection = factory.connect();
        }
        catch (DaoException e) {
            throw e;
        }
        PreparedStatement statement = null;
        ResultSet result = null;
        try {
            statement = DaoFactory.initPreparedStatement(connection,
                    true, FIND_BY_ID_SQL_QUERY, id);
            result = statement.executeQuery();
            if (result.next()) {
                question = map(result);
            }
            DaoFactory.closeSilently(connection, statement, result);
            return question;
        }
        catch (SQLException e) {
            DaoFactory.closeSilently(connection, statement, result);
            throw new DaoException("Something went wrong", e);
        }
    }

    public int count() {
        int count = 0;
        Connection connection = null;
        try {
            connection = factory.connect();
        }
        catch (DaoException e) {
            throw e;
        }
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            statement = DaoFactory.initPreparedStatement(connection,
                    true, COUNT_SQL_QUERY);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                count = resultSet.getInt(1);
            }
        }
        catch (SQLException e) {
            throw new DaoException("Something went wrong", e);
        }
        return count;
    }

    public void update(String answer, Question question) {
        Connection connection = null;
        try {
            connection = factory.connect();
        }
        catch (DaoException ex) {
            throw ex;
        }
        PreparedStatement statement = null;
        ResultSet generatedKeys = null;
        int status;
        try {
            statement = DaoFactory.initPreparedStatement(connection,
                    true, UPDATE_QUESTION_ANSWER_SQL_QUERY,
                    answer, question.getId());
        }
        catch (SQLException e) {
            throw new DaoException("Something wents wrong", e);
        }

        try {
            status = statement.executeUpdate();
        }
        catch (SQLException e) {
            throw new DaoException("Unable to update question.", e);
        }

        if (status == 0) {
            throw new DaoException("Unable to update question");
        }

        try {
            generatedKeys = statement.getGeneratedKeys();

            if (generatedKeys.next()) {
                question.setId(generatedKeys.getLong("id"));
            }
        }
        catch (SQLException e) {
            throw new DaoException("Unable to update question:"
                    + " no id fetched");
        }
        catch (DaoException e) {
            throw e;
        }
        DaoFactory.closeSilently(connection, statement, generatedKeys);
    }

    public void setTag(String tag, Question question) throws Exception {
//        DaoFactory factory = DaoFactory.getInstance();
//        TagDao tagDao = factory.getTagDao();
//        Tag tagObj = new Tag();
//        tagObj.setTag(tag);
//        tagDao.addTag(tagObj);
//        Connection connection = null;
//        try {
//            connection = factory.connect();
//        }
//        catch (DaoException ex) {
//            throw ex;
//        }
//        PreparedStatement statement = null;
//        ResultSet generatedKeys = null;
//        int status;
//        try {
//            statement = DaoFactory.initPreparedStatement(connection,
//                    true, ADD_TAG_QUERY,
//                    question.getId(),
//                    tagObj.getTag());
//        }
//        catch (SQLException e) {
//            throw new DaoException("Something wents wrong", e);
//        }
//
//        try {
//            status = statement.executeUpdate();
//        }
//        catch (SQLException e) {
//            DaoFactory.closeSilently(connection, statement, generatedKeys);
//            throw new DaoException("Unable to set tag.", e);
//        }
//
//        if (status == 0) {
//            DaoFactory.closeSilently(connection, statement, generatedKeys);
//            throw new DaoException("Unable to set tag");
//        }
//        DaoFactory.closeSilently(connection, statement, generatedKeys);
    }

//    @Override
//    public List<Question> findByTag(List<Tag> tags) throws DaoException {
//        return new ArrayList<>();
//    }

    @Override
    public void delete(Question question) throws DaoException {
        if (question == null) {
            throw new IllegalArgumentException();
        }
        Connection connection = null;
        try {
            connection = factory.connect();
        }
        catch (DaoException ex) {
            throw ex;
        }
        PreparedStatement statement = null;
        int status;
        try {
            statement = DaoFactory.initPreparedStatement(connection,
                    false, DELETE_QUESTION_SQL_QUERY,
                    question.getId());
        }
        catch (SQLException e) {
            throw new DaoException("Something wents wrong", e);
        }

        try {
            status = statement.executeUpdate();
        }
        catch (SQLException e) {
            throw new DaoException("Unable to delete question.", e);
        }

        if (status == 0) {
            throw new DaoException("Unable to delete question");
        }
        DaoFactory.closeSilently(connection, statement);
    }

    public Question map(ResultSet result) throws SQLException {
        Question question = new Question();

        question.setId(result.getLong("id"));
        question.setTitle(result.getString("title"));
        question.setBody(MdToHtml.render(result.getString("body")));
//        question.setTags(this.getTags(question));
        question.setAnswer(result.getString("answer"));
        question.setAuthor_email("author_email");
        question.setCreated_at(result.getString("created_at"));
        question.setUpdated_at(result.getString("updated_at"));
//        question.setUpdated_by(result.getLong("updated_by"));
        question.setAnswered_at(result.getString("answered_at"));
//        question.setAnswered_by(result.getLong("answered_by"));

        return question;
    }

    public List<Question> mapToList(ResultSet set) throws SQLException {
        List<Question> list = new ArrayList<>();

        while (set.next()) {
            list.add(map(set));
        }

        return list;
    }

    @Override
    public List<Question> findByTag(List<Tag> tags) throws DaoException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
