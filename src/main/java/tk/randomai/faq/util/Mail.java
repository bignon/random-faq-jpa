package tk.randomai.faq.util;

import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

/**
 *
 * @author kadou
 */
public class Mail {

    private static final String from = "faq@random.ai";
    private static final String host = "smtp.mailtrap.io";
    private static final String user = "b275ba3cda8934";
    private static final String pwd = "83b49d97c322c6";

    public static boolean sendMail(String to, String subject, String msg) {

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.port", "2525"); 
        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(user, pwd);
            }
        });

        try {
            // Create a default MimeMessage object.
            Message message = new MimeMessage(session);

            // Set From: header field
            message.setFrom(new InternetAddress(from));

            // Set To: header field
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(to));

            // Set Subject: header field
            message.setSubject(subject);

            // Config for HTML emails
            Multipart multipart = new MimeMultipart();
            MimeBodyPart bodyMessagePart = new MimeBodyPart();
            bodyMessagePart.setContent(msg, "text/html;charset=utf-8");
            multipart.addBodyPart(bodyMessagePart);

            // Put the content of your message
            message.setContent(multipart);

            // Send message
            Transport.send(message);

            System.out.println("Sent message successfully....");

        }
        catch (MessagingException e) {
            
            throw new RuntimeException(e);
        }
        return true;
    }
}
