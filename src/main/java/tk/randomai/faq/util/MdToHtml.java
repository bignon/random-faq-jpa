/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tk.randomai.faq.util;

import java.util.Arrays;
import java.util.List;
import org.commonmark.node.*;
import org.commonmark.parser.Parser;
import org.commonmark.renderer.html.HtmlRenderer;
import org.commonmark.ext.gfm.tables.TablesExtension;
import org.commonmark.ext.autolink.AutolinkExtension;
import org.commonmark.ext.gfm.strikethrough.StrikethroughExtension;
import org.commonmark.ext.ins.InsExtension;
import org.commonmark.Extension;
import org.owasp.html.HtmlPolicyBuilder;
import org.owasp.html.PolicyFactory;
import org.owasp.html.Sanitizers;

/**
 *
 * @author kadou
 */
public class MdToHtml {
    
    public static String render(String md) {
        return sanitize(toHtml(md));
    }
    
    public static String sanitize(String html) {
        
        PolicyFactory factory = Sanitizers.IMAGES
            .and(Sanitizers.TABLES)
            .and(Sanitizers.LINKS)
            .and(new HtmlPolicyBuilder()
                .allowCommonBlockElements()
                .allowCommonInlineFormattingElements()
                .disallowElements("script", "iframe")
                .disallowWithoutAttributes("a", "img")
                .allowElements("pre", "input")
                .allowUrlProtocols("http", "https",
                    "mailto", "tel")
                .requireRelNofollowOnLinks()
                .toFactory()
            );
        return factory.sanitize(html);
    }
    
    public static String toHtml(String md) {
        
        List<Extension> extensions = Arrays.asList(
            TablesExtension.create(), // gfm tables style
            AutolinkExtension.create(), // raw links to html links
            StrikethroughExtension.create(), // strikethrough with ~~ ~~
            InsExtension.create()); // underline with ++ ++
        Parser parser  = Parser.builder()
            .extensions(extensions)
            .build();
        HtmlRenderer renderer = HtmlRenderer.builder()
            .extensions(extensions)
            .build();
        Node document = parser.parse(md);
        return renderer.render(document);
    }
}
