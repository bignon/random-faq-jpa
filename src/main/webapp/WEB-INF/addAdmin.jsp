<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Ces trois balises meta *doivent* venir avant tout autre balise meta, c'est indispensable -->

    <title>RAi - Ajouter administrateur</title>

    <link rel="icon" type="image/png" href="../favicon.png" />
    <!-- Font Awesome Icon -->
    <link rel="stylesheet" href="../css/font-awesome.min.css">

    <!-- Feuille de style personnalise-->
    <link rel="stylesheet" href="../css/style_addAdmin.css">

    <script src="../js/main.js" defer></script>

    
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
</head>
<style>
    input::placeholder {
        opacity: 1;
        color: #2e2769;
    }
    .notif {
        min-height: 30px;
        position: relative;
        display: flex;
        justify-content: center;
        align-items: center;
        padding: 8px;
        background: #2e2769;
        color: whitesmoke;
        border-radius: 5px;
        border: none;
    }
    .close-notif {
        position: absolute;
        right: 5px;
        cursor: pointer;
    }
</style>
<body>
    <header class="main">
        <nav>
            <div class="logobox">  
                <a href="/"><img src="../img/logo.svg" alt=""></a>
                <a href="/" class="logo">randomAi/faq</a>
            </div>
            <div class="nav__links">
                <a href="dashboard" class="nav__items">Questions en attentes</a>
                <a href="administration" class="nav__items" style="border-bottom: 2px solid limegreen;">Administration</a>
                <a href="logout" class="nav__items logout"><i class="fa fa-power-off"></i></a>
            </div>
            <div class="toggle" id="toggler">
                <i class="fa fa-bars"></i>
            </div>

        </nav>
        <div class="dropdown" id="dropdown">
            <ul>
                <li class="drop-item"><a href="dashboard">Questions en attentes</a></li>
                <li class="drop-item"><a href="administration" style="color: rgba(60, 204, 248, 0.966) !important;">CrÃ©er administrateur</a></li>
                <li class="drop-item"><a href="logout" class="logout-drop">Se deconnecter</a></li>
            </ul>
        </div>
    </header>

    <div class="wrapper">
        <div class="container">
            <c:if test="${requestScope.notification != null}">
                <div id="notif" class="faq-accord notif" style="width: 100%; height: auto;position: relative">
                    <div>
                        ${requestScope.notification}
                    </div>
                    <span class="close-notif" onclick="removeNotif(event)" style="float: right; color: red;">
                        <i class="fa fa-window-close"></i>
                    </span>
                </div>
                    <br>
                    <br>
            </c:if>
            <h3>Ajouter administrateur</h3>
            <form action="" method="post">
                <input type="email" name="email" id="" placeholder="Email de l'administrateur">
                <input type="text" name="lname" id="" placeholder="Nom de l'administrateur">
                <input type="text" name="fname" id="" placeholder="Prenoms de l'administrateur">
                <input type="password" name="pwd1" id="" placeholder="Mot de passe de l'administrateur">
                <input type="password" name="pwd2" id="" placeholder="Confirmer le mot de passe de l'administrateur">
                <input type="submit" name="submit" class="btn" value="Ajouter">
            </form>
        </div>
    </div>
</body>
</html>