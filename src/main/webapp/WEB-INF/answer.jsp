<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <!-- Ces trois balises meta *doivent* venir avant tout autre balise meta, c'est indispensable -->

        <title>RAi - Repondre a une question</title>

        <link rel="icon" type="image/png" href="../favicon.png" />
        <!-- Font Awesome Icon -->
        <link rel="stylesheet" href="../css/font-awesome.min.css">

        <!-- Feuille de style personnalise-->
        <link rel="stylesheet" href="../css/style_edit.css">
        <link rel="stylesheet" href="../css/easymde.min.css">
        <link rel="stylesheet" href="../css/highlight/styles/shades-of-purple.css">
        <script src="../js/easymde.min.js"></script>
        <script src="../js/highlight/highlight.pack.js"></script>
        <script src="../js/main.js" defer></script>


        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
                <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
                <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
            <![endif]-->
    </head>
    <body>
        <header class="main">
            <nav>
                <div class="logobox">  
                    <a href="index.html"><img src="../img/logo.svg" alt=""></a>
                    <a href="index.html" class="logo">randomAi/faq</a>
                </div>
                <div class="nav__links">
                    <a href="dashboard" class="nav__items">Questions en attentes</a>
                    <a href="administration" class="nav__items">Administration</a>
                    <a href="logout" class="nav__items logout"><i class="fa fa-power-off"></i></a>
                </div>
                <div class="toggle" id="toggler">
                    <i class="fa fa-bars"></i>
                </div>

            </nav>
            <div class="dropdown" id="dropdown">
                <ul>
                    <li class="drop-item"><a href="waiting_questions.html">Questions en attentes</a></li>
                    <li class="drop-item"><a href="addAdmin.html">Creer administrateur</a></li>
                    <li class="drop-item"><a href="#" class="logout-drop">Se deconnecter</a></li>
                </ul>
            </div>
        </header>

        <div class="wrapper">
            <h1>
                <c:if test="${requestScope.question != null}">
                    ${question.title}
                </c:if>
            </h1>

            <!-- QUESTION -->
            <div class="question">
                <div class="content">
                    <c:if test="${requestScope.question != null}">
                        ${question.body}
                    </c:if>
                </div>
                <button>
                    <a style="width:100%; height: 100%; color: whitesmoke;
                        text-decoration: none;"
                        href="${contextPath}/management/delete?q=${ question.id }"
                    >
                    <i class="fa fa-trash"></i>
                    Supprimer la question
                    </a>
                </button>
            </div>

            <h1>Repondre</h1>
            <div class="barre"></div>
            <h3>Ajouter tag</h3>
            <form action="" method="post">
                <input type="hidden" name="q" value="${question.id}">
                <input type="text" name="tagging" id="taggin" 
                    placeholder="(ex: #visio #api #randomAi)"
                    required
                >
                <br>
                <textarea style="width: 100%; height: 200px;" name="answer" id=""
                          required
                >
                    
                </textarea>
                <br>
                <button type="submit" name="submit" class="btn">
                    Poster la reponse
                </button>

            </form>
            <br>
        </div>
        <div class="footer">
            <div>faq.randomai.info</div>
        </div>
        <script>
            document.addEventListener("DOMContentLoaded", function (event) {

                let editor = new EasyMDE({
                    forceSync: true,
                    initialValue: '# Ecrivez votre reponse ici',
                    placeholder: 'Détaillez votre question ici!',
                    maxHeight: "300px",
                    indentWithTabs: false,
                    lineNumbers: true,
                    imagePathAbsolute: true,
                    uploadImage: true,
                    previewImagesInEditor: true,
                    sideBySideFullscreen: true,
                    imageMaxSize: 1024 * 1024 * 30,
                    imageUploadEndpoint: "api/v1/image/upload",
                    renderingConfig: {
                        codeSyntaxHighlighting: true
                    }
                });

                hljs.highlightAll();
            });
        </script>
    </body>
</html>