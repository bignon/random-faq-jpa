<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <!-- Ces trois balises meta *doivent* venir avant tout autre balise meta, c'est indispensable -->

        <title>RAi - Questions en attentes</title>

        <link rel="icon" type="image/png" href="../favicon.png" />
        <!-- Font Awesome Icon -->
        <link rel="stylesheet" href="../css/font-awesome.min.css">

        <!-- Feuille de style personnalise-->
        <link rel="stylesheet" href="../css/style_wtg_question.css">\
        <link rel="stylesheet" href="../css/highlight/styles/shades-of-purple.css">
        <script src="../js/highlight/highlight.pack.js"></script>

        <script src="../js/main.js" defer></script>


        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
                <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
                <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
            <![endif]-->
    </head>
    <style>
        .content {
            font-size: 98%;
        }
        .content a {
            text-decoration: none;
            color: #2d2b57;
            transition: .2s;
        }

        .content a:visited, .content a:active {
            text-decoration: underline;
        }

        .content a:hover {
            border: 1px solid #2d2b57;
        }

        .content img {
            max-width: 100%;
            height: auto;
        }
    </style>
    <body>
        <header class="main">
            <nav>
                <div class="logobox">  
                    <a href="index.html"><img src="../img/logo.svg" alt=""></a>
                    <a href="index.html" class="logo">randomAi/faq</a>
                </div>
                <div class="nav__links">
                    <a href="dashboard" class="nav__items" style="border-bottom: 2px solid limegreen;">Questions en attentes</a>
                    <a href="administration" class="nav__items">Administration</a>
                    <a href="logout" class="nav__items logout"><i class="fa fa-power-off"></i></a>
                </div>
                <div class="toggle" id="toggler">
                    <i class="fa fa-bars"></i>
                </div>

            </nav>
            <div class="dropdown" id="dropdown">
                <ul>
                    <li class="drop-item">
                        <a href="waiting_questions.html" style="color: rgba(60, 204, 248, 0.966) !important;">
                            Questions en attentes
                        </a>
                    </li>
                    <li class="drop-item">
                        <a href="addAdmin.html">
                            Créer administrateur
                        </a>
                    </li>
                    <li class="drop-item">
                        <a href="#" class="logout-drop">
                            Se deconnecter
                        </a>
                    </li>
                </ul>
            </div>
        </header>

        <div class="wrapper" style="min-height: 100vh;">
            <c:if test="${sessionScope.notification != null}">
                <div class="faq-accord" style="position: relative;">
                    <div style="width: 100%; height: 100%;">
                        ${sessionScope.notification}
                    </div>
                    <span style="position: absolute; right: 5px; top: 5px; color: red;">
                        <i class="fa fa-window-close"></i>
                    </span>
                </div>
            </c:if>
            <h2>
                QUESTIONS EN ATTENTES
                <sup style"opacity: 0.75; color: #f90101; height: 20px; width: 20px;
                     border: none; border-radius: 50%; background: #01f901">
                    ${requestScope.total}
                </sup>
            </h2>
            <div class="barre"></div>
            <br>
            <c:forEach var="question" items="${requestScope.questions}">
                <div class="faq-accord">
                    <div class="acc">
                        <button class="accordion">
                            <span class="qTitle">
                                ${question.title}
                            </span>
                        </button>
                        <div class="panel">
                            <c:if test="${question.body}">
                                <<h3>Details</h3>
                            </c:if>
                            <div class="content" style="padding: 10px;">
                                ${question.body}
                            </div>
                            <div class="operation">
                                <button class="op-btn answer-btn">
                                    <a style="width:100%; height: 100%;"
                                       href="${contextPath}/management/answer?q=${question.id }"
                                       >
                                        Repondre
                                    </a>
                                </button>
                                <button class="op-btn delete-btn">
                                    <a style="width:100%; height: 100%;" 
                                       href="${contextPath}/management/delete?q=${question.id }"
                                       >
                                        Supprimer
                                    </a>
                                </button>
                            </div>
                        </div>
                    </div>

                </div>
            </c:forEach>
        </div>
        <div class="footer">
            <div>faq.randomai.info</div>
        </div>

        <script>
            // Highlight code blocks
            hljs.highlightAll();
            var acc = document.getElementsByClassName("accordion");
            var i;

            for (i = 0; i < acc.length; i++) {
                acc[i].addEventListener("click", function () {
                    this.classList.toggle("active");
                    var panel = this.nextElementSibling;
                    if (panel.style.display === "flex") {
                        panel.style.display = "none";
                    } else {
                        panel.style.display = "flex";
                    }
                });
            }
        </script>

    </body>
</html>